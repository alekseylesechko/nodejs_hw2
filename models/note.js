import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const note = new Schema({
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

export const Note = model('Note', note);
