import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const user = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  notesId: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Note',
      required: true,
    },
  ],
});

user.methods.addNote = function(note) {
  const notes = [...this.notesId];
  notes.push(note._id);
  this.notesId = notes;
  return this.save();
};

user.methods.checkNoteId = function(id) {
  return this.notesId.some((note) => note._id.toString() === id);
};

user.methods.removeNote = function(id) {
  let notes = [...this.notesId];

  const note = notes.find((note) => note.toString() === id);
  if (!note) {
    return false;
  }
  notes = notes.filter((note) => note.toString() !== id);
  this.notesId = notes;
  return this.save();
};

export const User = model('User', user);
