import mongoose from 'mongoose';
const {Schema, model} = mongoose;

const credentials = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

credentials.method('toClient', function() {
  // eslint-disable-next-line
  const credentials = this.toObject();

  delete credentials.__v;
  return credentials;
});

export const Credentials = model('Credentials', credentials);
