import jwt from 'jsonwebtoken';

export function getToken(id) {
  return jwt.sign({_id: id}, process.env.TOKEN_SECRET);
}

export function verifyToken(token) {
  try {
    return jwt.verify(token, process.env.TOKEN_SECRET);
  } catch (e) {
    return false;
  }
}
