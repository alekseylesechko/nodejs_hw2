export function mapNoteItems(notes) {
  return notes.map((note) => ({
    ...note._doc,
  }));
}

export function getById(notes, id) {
  const note = mapNoteItems(notes).find((note) => note._id.toString() === id);
  if (!note) {
    return null;
  }
  delete note.__v;
  return note;
}
