import Joi from 'joi';

const authSchema = Joi.object({
  username: Joi.string().min(3).required(),
  password: Joi.string().min(3).required(),
});

const noteSchema = Joi.object({
  text: Joi.string().min(3).required(),
});


const changePasswordSchema = Joi.object({
  oldPassword: Joi.string().min(3).required(),
  newPassword: Joi.string().min(3).required(),
});

export function validateAuth(body) {
  return authSchema.validate(body);
}

export function validateNote(body) {
  return noteSchema.validate(body);
}


export function validateChangingPassword(body) {
  return changePasswordSchema.validate(body);
}
