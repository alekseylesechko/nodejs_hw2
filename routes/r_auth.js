import express from 'express';
import {register, login} from '../controllers/c_auth.js';
import {
  checkAuthParams,
  checkUserExists,
  asyncWrapper,
} from './middlewares/middlewares.js';

// eslint-disable-next-line
export const router = express.Router();

// POST /api/auth/register
router.post(
    '/register',
    checkAuthParams,
    checkUserExists,
    asyncWrapper(register),
);

// POST /api/auth/login
router.post('/login', checkAuthParams, asyncWrapper(login));
