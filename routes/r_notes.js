import express from 'express';
import {
  getAllNotes,
  addNotes,
  getNotesById,
  patchNotes,
  putNotes,
  deleteNotes,
} from '../controllers/c_notes.js';
import {
  checkToken,
  checkNoteParams,
  asyncWrapper,
} from './middlewares/middlewares.js';

// eslint-disable-next-line
export const router = express.Router();
router.use(asyncWrapper(checkToken));

// GET /api/notes
router.get('/', asyncWrapper(getAllNotes));

// POST /api/notes
router.post('/', checkNoteParams, asyncWrapper(addNotes));

// GET /api/notes/{id}
router.get('/:id', asyncWrapper(getNotesById));

// PATCH /api/notes/{id}
router.patch('/:id', asyncWrapper(patchNotes));

// PUT /api/notes/{id}
router.put('/:id', checkNoteParams, asyncWrapper(putNotes));

// DELETE /api/notes/{id}
router.delete('/:id', asyncWrapper(deleteNotes));
