import fs from 'fs';
import {
  validateAuth,
  verifyToken,
  validateNote,
  validateChangingPassword,
  compareHashPassword,
} from '../../helpers/index.js';
import {User, Credentials} from '../../models/index.js';

const writeStream = fs.createWriteStream('log.txt');

export function addInfo(req, res, next) {
  const info = {
    date: new Date().toLocaleString(),
    method: req.method,
    url: req.url,
    data: req.body,
  };

  writeStream.write(JSON.stringify(info) + '\n');
  next();
}

export async function checkAuthParams(req, res, next) {
  const validate = validateAuth(req.body);

  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkUserExists(req, res, next) {
  const userExists = await User.findOne({username: req.body.username});
  if (userExists) {
    return res.status(400).json({message: 'User already exists'});
  }
  next();
}

export async function checkToken(req, res, next) {
  const token = req.headers['authorization']?.split(' ')[1];
  if (!token) {
    return res.status(400).json({message: 'Access denied'});
  }
  const varify = verifyToken(token);
  if (!varify) {
    return res.status(400).json({message: 'Wrong token'});
  }
  const userCredentials = await Credentials.findOne({_id: varify._id});
  if (!userCredentials) {
    return res.status(400).json({message: 'User not exists'});
  }
  req.user = await User.findOne({username: userCredentials.username});
  req.credentialsId = userCredentials._id;
  next();
}

export async function checkNoteParams(req, res, next) {
  const validate = validateNote(req.body);

  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkChangePasswordParam(req, res, next) {
  const validate = validateChangingPassword(req.body);
  if (validate.error) {
    return res.status(400).json({message: validate.error.details[0].message});
  }
  next();
}

export async function checkOldPassword(req, res, next) {
  try {
    const userCredentials = await Credentials.findOne({
      username: req.user.username,
    });
    const isPasswordValid = await compareHashPassword(
        req.body.oldPassword,
        userCredentials.password,
    );
    if (!isPasswordValid) {
      return res.status(400).json({message: 'Old password wrong'});
    }
    next();
  } catch (err) {
    {
      return res.status(500).json({message: err.message || 'Server error'});
    }
  }
}

export function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
}
