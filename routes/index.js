export {router as authRouter} from './r_auth.js';
export {router as notesRouter} from './r_notes.js';
export {router as usersRouter} from './r_users.js';
