import express from 'express';
import {getUser, deleteUser, updatePassword} from '../controllers/c_users.js';
import {
  checkToken,
  checkChangePasswordParam,
  checkOldPassword,
  asyncWrapper,
} from './middlewares/middlewares.js';

// eslint-disable-next-line
export const router = express.Router();
router.use(asyncWrapper(checkToken));

// GET api/users/me
router.get('/', asyncWrapper(getUser));

// PATCH api/users/me
router.patch(
    '/',
    checkChangePasswordParam,
    checkOldPassword,
    asyncWrapper(updatePassword),
);

// DELETE api/users/me
router.delete('/', asyncWrapper(deleteUser));
