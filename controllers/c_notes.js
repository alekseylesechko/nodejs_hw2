import {Note} from '../models/index.js';
import {getById} from '../helpers/index.js';

export const getAllNotes = async (req, res) => {
  const userId = await req.user._id;
  const skip = +req.query.offset;
  const limit = +req.query.limit;
  const notes = await Note.find({userId}, {__v: 0}, {skip, limit});
  res.status(200).json({
    notes,
  });
};

export const addNotes = async (req, res) => {
  const note = new Note({
    userId: req.user,
    completed: false,
    text: req.body.text,
  });
  await note.save();
  await req.user.addNote(note);
  res.status(200).json({message: 'Success'});
};

export const getNotesById = async (req, res) => {
  const user = await req.user.populate('notesId').execPopulate();
  const id = req.params.id;
  const note = getById(user.notesId, id);
  if (!note) {
    return res.status(400).json({message: 'Note is not defined'});
  }
  return res.status(200).json({
    note,
  });
};

export const patchNotes = async (req, res) => {
  const user = await req.user.populate('notesId').execPopulate();
  const id = req.params.id;
  const note = getById(user.notesId, id);
  if (!note) {
    return res.status(400).json({message: 'Note is not defined'});
  }
  await Note.findByIdAndUpdate(id, {completed: !note.completed});
  return res.status(200).json({message: 'Success'});
};

export const putNotes = async (req, res) => {
  const text = req.body.text;
  const id = req.params.id;
  const isNoteExists = await req.user.checkNoteId(id);
  if (!isNoteExists) {
    return res.status(400).json({message: 'Note is not defined'});
  }
  await Note.findByIdAndUpdate(id, {text});
  return res.status(200).json({message: 'Success'});
};

export const deleteNotes = async (req, res) => {
  const isDeleted = await req.user.removeNote(req.params.id);
  await Note.findByIdAndDelete(req.params.id);
  if (isDeleted) {
    return res.status(200).json({message: 'Success'});
  }
  return res.status(400).json({message: 'Note not found'});
};
