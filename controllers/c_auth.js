import {User, Credentials} from '../models/index.js';
import {
  createHashPassword,
  compareHashPassword,
  getToken,
} from '../helpers/index.js';

export const register = async (req, res) => {
  const password = await createHashPassword(req.body.password);

  const user = new User({
    username: req.body.username,
    notes: [],
  });
  const credentials = new Credentials({
    username: req.body.username,
    password,
  });

  await user.save();
  await credentials.save();
  return res.status(200).json({message: 'Success'});
};

export const login = async (req, res) => {
  const userCredentials = await Credentials.findOne({
    username: req.body.username,
  });
  if (!userCredentials) {
    return res.status(400).json({message: 'User not found'});
  }
  const isPasswordValid = await compareHashPassword(
      req.body.password,
      userCredentials.password,
  );
  if (!isPasswordValid) {
    return res.status(400).json({message: 'Password wrong'});
  }
  const token = getToken(userCredentials._id);

  return res.status(200).json({
    message: 'Success',
    jwt_token: token,
  });
};
