import {User, Credentials} from '../models/index.js';
import {createHashPassword} from '../helpers/index.js';

export const getUser = async (req, res) => {
  const user = await User.findOne({_id: req.user._id}, {__v: 0, notesId: 0});
  if (user) {
    return res.status(200).json({user});
  }
  return res.status(400).json({message: 'User not found'});
};

export const updatePassword = async (req, res) => {
  const password = await createHashPassword(req.body.newPassword);
  await Credentials.findByIdAndUpdate(req.credentialsId, {
    password,
  });
  return res.status(200).json({message: 'Success'});
};

export const deleteUser = async (req, res) => {
  const user = await User.findOneAndDelete({_id: req.user._id});
  if (user) {
    await Credentials.findByIdAndDelete(req.credentialsId);
    return res.status(200).json({message: 'Success'});
  }
  return res.status(400).json({message: 'User not found'});
};
