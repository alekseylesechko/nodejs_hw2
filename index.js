import dotenv from 'dotenv';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import {authRouter, notesRouter, usersRouter} from './routes/index.js';
import {addInfo} from './routes/middlewares/middlewares.js';

dotenv.config();
const app = express();
const PORT = process.env.PORT || 8080;

async function start() {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch (e) {
    console.log(e);
  }
}

start();

// Middleware
app.use(cors());
app.use(express.json());
app.use(addInfo);

// Route middlewares
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

// eslint-disable-next-line
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
